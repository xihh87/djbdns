#include "auto_home.h"

void hier()
{
  c("/","etc","dnsroots.global",-1,-1,0644);

  h(auto_home,-1,-1,02755);
  d(auto_home,"command",-1,-1,02755);

  c(auto_home,"command","dnscache-conf",-1,-1,0755);
  c(auto_home,"command","tinydns-conf",-1,-1,0755);
  c(auto_home,"command","walldns-conf",-1,-1,0755);
  c(auto_home,"command","rbldns-conf",-1,-1,0755);
  c(auto_home,"command","pickdns-conf",-1,-1,0755);
  c(auto_home,"command","axfrdns-conf",-1,-1,0755);

  c(auto_home,"command","dnscache",-1,-1,0755);
  c(auto_home,"command","tinydns",-1,-1,0755);
  c(auto_home,"command","walldns",-1,-1,0755);
  c(auto_home,"command","rbldns",-1,-1,0755);
  c(auto_home,"command","pickdns",-1,-1,0755);
  c(auto_home,"command","axfrdns",-1,-1,0755);

  c(auto_home,"command","tinydns-get",-1,-1,0755);
  c(auto_home,"command","tinydns-data",-1,-1,0755);
  c(auto_home,"command","tinydns-edit",-1,-1,0755);
  c(auto_home,"command","rbldns-data",-1,-1,0755);
  c(auto_home,"command","pickdns-data",-1,-1,0755);
  c(auto_home,"command","axfr-get",-1,-1,0755);

  c(auto_home,"command","dnsip",-1,-1,0755);
  c(auto_home,"command","dnsipq",-1,-1,0755);
  c(auto_home,"command","dnsname",-1,-1,0755);
  c(auto_home,"command","dnstxt",-1,-1,0755);
  c(auto_home,"command","dnsmx",-1,-1,0755);
  c(auto_home,"command","dnsfilter",-1,-1,0755);
  c(auto_home,"command","random-ip",-1,-1,0755);
  c(auto_home,"command","dnsqr",-1,-1,0755);
  c(auto_home,"command","dnsq",-1,-1,0755);
  c(auto_home,"command","dnstrace",-1,-1,0755);
  c(auto_home,"command","dnstracesort",-1,-1,0755);
}
